package com.gergo.takacs.tsuro.console

import com.gergo.takacs.tsuro.Card
import com.gergo.takacs.tsuro.EmptyTile
import com.gergo.takacs.tsuro.MarkerPosition
import com.gergo.takacs.tsuro.SidePosition
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class TileRendererTest {

    @Test
    fun shouldRenderAnEmptyTileWithTopLeftMarker() {
        val emptyTile = EmptyTile()
        val marker = MarkerPosition(0, 0, SidePosition.TOP_LEFT, "@")
        val renderTile = TileRenderer().renderTile(emptyTile, listOf(marker), "A1")
        assertEquals(
            listOf(
                "┌───@───┼───┐",
                "│           │",
                "┼           ┼",
                "│    A1     │",
                "┼           ┼",
                "│           │",
                "└───┼───┼───┘"
            ), renderTile
        )
    }

    @Test
    fun shouldRenderAnEmptyTileWithTopRightMarker() {
        val emptyTile = EmptyTile()
        val marker = MarkerPosition(0, 0, SidePosition.TOP_RIGHT, "@")
        val renderTile = TileRenderer().renderTile(emptyTile, listOf(marker), "A1")
        assertEquals(
            listOf(
                "┌───┼───@───┐",
                "│           │",
                "┼           ┼",
                "│    A1     │",
                "┼           ┼",
                "│           │",
                "└───┼───┼───┘"
            ), renderTile
        )
    }

    @Test
    fun shouldRenderAnEmptyTileWithBottomLeftMarker() {
        val emptyTile = EmptyTile()
        val marker = MarkerPosition(0, 0, SidePosition.BOTTOM_LEFT, "@")
        val renderTile = TileRenderer().renderTile(emptyTile, listOf(marker), "A1")
        assertEquals(
            listOf(
                "┌───┼───┼───┐",
                "│           │",
                "┼           ┼",
                "│    A1     │",
                "┼           ┼",
                "│           │",
                "└───@───┼───┘"
            ), renderTile
        )
    }

    @Test
    fun shouldRenderAnEmptyTileWithBottomRightMarker() {
        val emptyTile = EmptyTile()
        val marker = MarkerPosition(0, 0, SidePosition.BOTTOM_RIGHT, "@")
        val renderTile = TileRenderer().renderTile(emptyTile, listOf(marker), "A1")
        assertEquals(
            listOf(
                "┌───┼───┼───┐",
                "│           │",
                "┼           ┼",
                "│    A1     │",
                "┼           ┼",
                "│           │",
                "└───┼───@───┘"
            ), renderTile
        )
    }

    @Test
    fun shouldRenderAnEmptyTileWithRightTopMarker() {
        val emptyTile = EmptyTile()
        val marker = MarkerPosition(0, 0, SidePosition.RIGHT_TOP, "@")
        val renderTile = TileRenderer().renderTile(emptyTile, listOf(marker), "A1")
        assertEquals(
            listOf(
                "┌───┼───┼───┐",
                "│           │",
                "┼           @",
                "│    A1     │",
                "┼           ┼",
                "│           │",
                "└───┼───┼───┘"
            ), renderTile
        )
    }

    @Test
    fun shouldRenderAnEmptyTileWithLeftTopMarker() {
        val emptyTile = EmptyTile()
        val marker = MarkerPosition(0, 0, SidePosition.LEFT_TOP, "@")
        val renderTile = TileRenderer().renderTile(emptyTile, listOf(marker), "A1")
        assertEquals(
            listOf(
                "┌───┼───┼───┐",
                "│           │",
                "@           ┼",
                "│    A1     │",
                "┼           ┼",
                "│           │",
                "└───┼───┼───┘"
            ), renderTile
        )
    }

    @Test
    fun shouldRenderAnEmptyTileWithLeftBottomMarker() {
        val emptyTile = EmptyTile()
        val marker = MarkerPosition(0, 0, SidePosition.LEFT_BOTTOM, "@")
        val renderTile = TileRenderer().renderTile(emptyTile, listOf(marker), "A1")
        assertEquals(
            listOf(
                "┌───┼───┼───┐",
                "│           │",
                "┼           ┼",
                "│    A1     │",
                "@           ┼",
                "│           │",
                "└───┼───┼───┘"
            ), renderTile
        )
    }

    @Test
    fun shouldRenderAnEmptyTileWithRightBottomMarker() {
        val emptyTile = EmptyTile()
        val marker = MarkerPosition(0, 0, SidePosition.RIGHT_BOTTOM, "@")
        val renderTile = TileRenderer().renderTile(emptyTile, listOf(marker), "A1")
        assertEquals(
            listOf(
                "┌───┼───┼───┐",
                "│           │",
                "┼           ┼",
                "│    A1     │",
                "┼           @",
                "│           │",
                "└───┼───┼───┘"
            ), renderTile
        )
    }

    @Test
    fun shouldRenderAnEmptyTileWithMultipleMarkersOnIt() {
        val emptyTile = EmptyTile()
        val markers = listOf(
            MarkerPosition(0, 0, SidePosition.RIGHT_BOTTOM, "@"),
            MarkerPosition(0, 0, SidePosition.RIGHT_TOP, "&"),
            MarkerPosition(0, 0, SidePosition.TOP_RIGHT, "#"),
            MarkerPosition(0, 0, SidePosition.TOP_LEFT, "$"),
            MarkerPosition(0, 0, SidePosition.BOTTOM_RIGHT, "%"),
            MarkerPosition(0, 0, SidePosition.BOTTOM_LEFT, "*"),
            MarkerPosition(0, 0, SidePosition.LEFT_BOTTOM, "^"),
            MarkerPosition(0, 0, SidePosition.LEFT_TOP, "!"),
        )
        val renderTile = TileRenderer().renderTile(emptyTile, markers, "A1")
        assertEquals(
            listOf(
                "┌───$───#───┐",
                "│           │",
                "!           &",
                "│    A1     │",
                "^           @",
                "│           │",
                "└───*───%───┘"
            ), renderTile
        )
    }

    @Test
    fun shouldRenderCardWithConnections() {
        val card = Card(
            listOf(
                SidePosition.LEFT_TOP to SidePosition.TOP_LEFT,
                SidePosition.TOP_RIGHT to SidePosition.BOTTOM_LEFT,
                SidePosition.RIGHT_TOP to SidePosition.RIGHT_BOTTOM,
                SidePosition.LEFT_BOTTOM to SidePosition.BOTTOM_RIGHT
            )
        )
        val renderTile = TileRenderer().renderTile(card, listOf())
        assertEquals(
            listOf(
                "┌───1───2───┐",
                "│           │",
                "1           3",
                "│           │",
                "4           3",
                "│           │",
                "└───2───4───┘"
            ),
            renderTile
        )
    }
}