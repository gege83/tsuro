package com.gergo.takacs.tsuro.console

import com.gergo.takacs.renderGame
import com.gergo.takacs.tsuro.*
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.containsString
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import java.io.BufferedReader
import java.io.ByteArrayOutputStream
import java.io.InputStreamReader
import java.io.PrintStream

internal class InitializeGameTest {
    @Test
    fun shouldAskForGameNameAndSetIt() {
        val gameName = "myGame"
        val player1Name = "Carly"
        val player2Name = "Gergo"
        val inputStream = "$gameName\n$player1Name\n$player2Name\n\n".byteInputStream()
        val outputStream = ByteArrayOutputStream()

        val game = initializeGame(BufferedReader(InputStreamReader(inputStream)), PrintStream(outputStream))
        assertEquals(gameName, game.name)
        val GAME_NAME = "Please enter the game name:"
        val PLAYER1_NAME = "Please enter the player1's name"
        val PLAYER2_NAME = "Please enter the player2's name"
        val PLAYER3_NAME = "Please enter the player3's name"
        assertEquals(
            "$GAME_NAME\n$PLAYER1_NAME\n$PLAYER2_NAME\n$PLAYER3_NAME\n",
            String(outputStream.toByteArray())
        )
        assertEquals(listOf(Player(player1Name, "@"), Player(player2Name, "#")), game.players)
    }

    private val drawingPile = DrawingPile()

    @Test
    internal fun shouldAskForPlacingMarkers() {
        val inputStream = "A2\n5\nB1\n1".byteInputStream()
        val outputStream = ByteArrayOutputStream()
        val players = listOf(Player("p1", "@"), Player("p2", "#"))
        val markerPositions = listOf(
            MarkerPosition(0, 1, SidePosition.BOTTOM_RIGHT, "@"),
            MarkerPosition(1, 0, SidePosition.TOP_LEFT, "#")
        )
        val game = Game("newGame", Board(2, 2), drawingPile, players)
        val gameWithMarkers =
            game.initializeMarkers(BufferedReader(InputStreamReader(inputStream)), PrintStream(outputStream))
        val output = String(outputStream.toByteArray())
        assertThat(output, containsString("p1 select the tile for your marker"))
        assertThat(output, containsString("please select side position"))
        assertThat(output, containsString("p2 select the tile for your marker"))
        assertThat(output, containsString("please select side position"))
        assertEquals(Game("newGame", Board(2, 2, markerPositions), drawingPile, players, 0), gameWithMarkers)
    }

    @Test
    internal fun shouldOnlyShowValidSides() {
        val inputStream = "A2\n5\nB1\n1".byteInputStream()
        val outputStream = ByteArrayOutputStream()
        val players = listOf(Player("p1", "@"), Player("p2", "#"))
        val markerPositions = listOf(
            MarkerPosition(0, 1, SidePosition.BOTTOM_RIGHT, "@"),
            MarkerPosition(1, 0, SidePosition.TOP_LEFT, "#")
        )
        val game = Game("newGame", Board(2, 2), drawingPile, players)
        val gameWithMarkers =
            game.initializeMarkers(BufferedReader(InputStreamReader(inputStream)), PrintStream(outputStream))
        val output = String(outputStream.toByteArray())
        assertThat(output, containsString("p1 select the tile for your marker"))
        assertThat(
            output,
            containsString(
                "please select side position\n${
                    getOption(SidePosition.BOTTOM_RIGHT)
                }${
                    getOption(SidePosition.BOTTOM_LEFT)
                }${
                    getOption(SidePosition.LEFT_BOTTOM)
                }${
                    getOption(SidePosition.LEFT_TOP)
                }"
            )
        )
        assertThat(output, containsString("p2 select the tile for your marker"))
        assertThat(
            output, containsString(
                "please select side position\n${
                    getOption(SidePosition.TOP_LEFT)
                }${
                    getOption(SidePosition.TOP_RIGHT)
                }${
                    getOption(SidePosition.RIGHT_TOP)
                }${
                    getOption(SidePosition.RIGHT_BOTTOM)
                }"
            )
        )
        assertEquals(Game("newGame", Board(2, 2, markerPositions), drawingPile, players, 0), gameWithMarkers)
    }

    private fun getOption(sidePosition: SidePosition) = "${sidePosition.ordinal + 1}. ${sidePosition.name}\n"

    @Test
    internal fun shouldReaskTheQuestionWhenInvalidTileIsSelected() {
        val inputStream = "B2\nB3\n5".byteInputStream()
        val outputStream = ByteArrayOutputStream()
        val players = listOf(Player("p1", "@"))
        val markerPositions = listOf(
            MarkerPosition(1, 2, SidePosition.BOTTOM_RIGHT, "@")
        )
        val gameName = "newGame"
        val game = Game(gameName, Board(3, 3), drawingPile, players)
        val gameWithMarkers =
            game.initializeMarkers(BufferedReader(InputStreamReader(inputStream)), PrintStream(outputStream))
        val output = String(outputStream.toByteArray())
        val pleasePlaceMarker = "p1 select the tile for your marker"
        assertThat(output, containsString(pleasePlaceMarker))
        assertThat(output, containsString("No valid move possible on the tile\n${pleasePlaceMarker}"))
        assertThat(output, containsString("please select side position"))
        assertEquals(Game(gameName, Board(3, 3, markerPositions), drawingPile, players, 0), gameWithMarkers)
    }

    @Test
    internal fun shouldReaskTheQuestionWhenInvalidSidePositionIsSelected() {
        val inputStream = "B1\n3\nB1\n9\nB1\n1".byteInputStream()
        val outputStream = ByteArrayOutputStream()
        val players = listOf(Player("p1", "@"))
        val markerPositions = listOf(
            MarkerPosition(1, 0, SidePosition.TOP_LEFT, "@")
        )
        val gameName = "newGame"
        val game = Game(gameName, Board(3, 3), drawingPile, players)
        val gameWithMarkers =
            game.initializeMarkers(BufferedReader(InputStreamReader(inputStream)), PrintStream(outputStream))
        val output = String(outputStream.toByteArray())
        val pleasePlaceMarker = "p1 select the tile for your marker"
        assertThat(output, containsString(pleasePlaceMarker))
        assertThat(output, containsString("please select side position"))
        assertThat(output, containsString("selected position is not valid\n${pleasePlaceMarker}"))
        assertEquals(Game(gameName, Board(3, 3, markerPositions), drawingPile, players, 0), gameWithMarkers)
    }

    @Test
    internal fun shouldAcceptLowerCaseLettersWhenSelectingTile() {
        val inputStream = "b1\n1".byteInputStream()
        val outputStream = ByteArrayOutputStream()
        val players = listOf(Player("p1", "@"))
        val markerPositions = listOf(
            MarkerPosition(1, 0, SidePosition.TOP_LEFT, "@")
        )
        val gameName = "newGame"
        val game = Game(gameName, Board(3, 3), drawingPile, players)
        val gameWithMarkers =
            game.initializeMarkers(BufferedReader(InputStreamReader(inputStream)), PrintStream(outputStream))
        val output = String(outputStream.toByteArray())
        val pleasePlaceMarker = "p1 select the tile for your marker"
        assertThat(output, containsString(pleasePlaceMarker))
        assertThat(output, containsString("please select side position"))
        assertEquals(Game(gameName, Board(3, 3, markerPositions), drawingPile, players, 0), gameWithMarkers)
    }

    @Test
    internal fun shouldShowTheFirstPlayersHandAfterAllPlayerSelectedStartingPosition() {
        val outputStream = ByteArrayOutputStream()
        val p1 = Player(
            "p1",
            "@",
            listOf(
                Card(
                    listOf(
                        SidePosition.TOP_LEFT to SidePosition.TOP_RIGHT,
                        SidePosition.RIGHT_TOP to SidePosition.RIGHT_BOTTOM,
                        SidePosition.BOTTOM_LEFT to SidePosition.BOTTOM_RIGHT,
                        SidePosition.LEFT_BOTTOM to SidePosition.LEFT_TOP
                    )
                ),
                Card(
                    listOf(
                        SidePosition.TOP_LEFT to SidePosition.RIGHT_BOTTOM,
                        SidePosition.RIGHT_TOP to SidePosition.BOTTOM_RIGHT,
                        SidePosition.BOTTOM_LEFT to SidePosition.LEFT_TOP,
                        SidePosition.LEFT_BOTTOM to SidePosition.TOP_RIGHT
                    )
                ),
                Card(
                    listOf(
                        SidePosition.TOP_LEFT to SidePosition.BOTTOM_RIGHT,
                        SidePosition.RIGHT_TOP to SidePosition.LEFT_TOP,
                        SidePosition.BOTTOM_LEFT to SidePosition.TOP_RIGHT,
                        SidePosition.LEFT_BOTTOM to SidePosition.RIGHT_BOTTOM
                    )
                ),
            )
        )
        val p2 = Player("p2", "#")
        val players = listOf(p1, p2)
        val gameName = "newGame"
        val game = Game(gameName, Board(3, 3), drawingPile, players)
        game.renderGame(PrintStream(outputStream))
        val output = String(outputStream.toByteArray())
        assertThat(output, containsString("p1's hand:"))
        assertThat(
            output, containsString(
                listOf(
                    "┌───1───1───┐┌───1───4───┐┌───1───3───┐",
                    "│           ││           ││           │",
                    "4           23           22           2",
                    "│           ││           ││           │",
                    "4           24           14           4",
                    "│           ││           ││           │",
                    "└───3───3───┘└───3───2───┘└───3───1───┘"
                ).joinToString("\n")
            )
        )
    }
}