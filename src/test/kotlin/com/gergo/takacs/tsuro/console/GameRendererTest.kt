package com.gergo.takacs.tsuro.console

import com.gergo.takacs.tsuro.*
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers.containsString
import org.junit.jupiter.api.Test
import org.mockito.ArgumentMatchers.anyList
import org.mockito.kotlin.*

internal class GameRendererTest {

    @Test
    fun shouldShowPlayersAndTheirsSymbolsAndTheActivePlayerAndTheBoard() {
        val mockBoardRenderer = mock<BoardRenderer> {
            on { renderBoard(any()) } doReturn "board"
        }
        val board = Board(2, 2)
        val game = Game(
            "myGame",
            board,
            DrawingPile(),
            listOf(
                Player("player1", "@"),
                Player("player2", "#"),
                Player("player3", "$")
            ),
            2
        )
        val mockTileRenderer = mock<TileRenderer> {
            on { renderTile(any(), anyList(), any()) } doReturn listOf("rendered tile")
        }
        val printedGame = GameRenderer(mockBoardRenderer, mockTileRenderer).renderGame(game)
        assertThat(printedGame, containsString("Players:"))
        assertThat(printedGame, containsString("   @ player1"))
        assertThat(printedGame, containsString("   # player2"))
        assertThat(printedGame, containsString("-> $ player3"))
        assertThat(printedGame, containsString("board"))
        verify(mockBoardRenderer).renderBoard(board)
    }

    @Test
    fun shouldShowActivePlayerHand() {
        val mockBoardRenderer = mock<BoardRenderer> {
            on { renderBoard(any()) } doReturn "board"
        }
        val board = Board(2, 2)
        val card = Card(listOf())
        val game = Game(
            "myGame",
            board,
            DrawingPile(),
            listOf(
                Player("player1", "@", listOf(card, card, card)),
                Player("player2", "#"),
                Player("player3", "$")
            ),
            0
        )
        val line1 = "rendered tile line1"
        val line2 = "rendered tile line2"
        val mockTileRenderer = mock<TileRenderer> {
            on { renderTile(any(), anyList(), any()) } doReturn listOf(line1, line2)
        }
        val printedGame = GameRenderer(mockBoardRenderer, mockTileRenderer).renderGame(game)
        assertThat(printedGame, containsString("player1's hand:"))
        assertThat(printedGame, containsString(line1 + line1 + line1 + "\n" + line2 + line2 + line2 + "\n"))

        verify(mockTileRenderer, times(3)).renderTile(card, listOf())
    }
}