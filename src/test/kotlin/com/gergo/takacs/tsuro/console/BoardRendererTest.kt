package com.gergo.takacs.tsuro.console

import com.gergo.takacs.tsuro.Board
import com.gergo.takacs.tsuro.MarkerPosition
import com.gergo.takacs.tsuro.SidePosition
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

internal class BoardRendererTest {

    @Test
    fun renderBoard() {
        val board = Board(2, 2, listOf(MarkerPosition(0, 0, SidePosition.LEFT_TOP, "$")))
        val printedBoard = BoardRenderer(TileRenderer()).renderBoard(board)
        assertEquals(
            "" +
                    "┌───┼───┼───┐┌───┼───┼───┐\n" +
                    "│           ││           │\n" +
                    "$           ┼┼           ┼\n" +
                    "│    A1     ││    B1     │\n" +
                    "┼           ┼┼           ┼\n" +
                    "│           ││           │\n" +
                    "└───┼───┼───┘└───┼───┼───┘\n" +
                    "┌───┼───┼───┐┌───┼───┼───┐\n" +
                    "│           ││           │\n" +
                    "┼           ┼┼           ┼\n" +
                    "│    A2     ││    B2     │\n" +
                    "┼           ┼┼           ┼\n" +
                    "│           ││           │\n" +
                    "└───┼───┼───┘└───┼───┼───┘\n" +
                    "",
            printedBoard
        )
    }
}