package com.gergo.takacs.tsuro

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertNotEquals
import org.junit.jupiter.api.Test

internal class DrawingPileTest {
    @Test
    internal fun `draw from the top of the pile`() {
        val card1 = Card(
            listOf(
                SidePosition.TOP_LEFT to SidePosition.BOTTOM_LEFT,
                SidePosition.TOP_RIGHT to SidePosition.RIGHT_TOP,
                SidePosition.RIGHT_BOTTOM to SidePosition.LEFT_BOTTOM,
                SidePosition.BOTTOM_RIGHT to SidePosition.LEFT_TOP
            )
        )
        val card2 = Card(
            listOf(
                SidePosition.TOP_LEFT to SidePosition.RIGHT_TOP,
                SidePosition.TOP_RIGHT to SidePosition.BOTTOM_LEFT,
                SidePosition.RIGHT_BOTTOM to SidePosition.LEFT_TOP,
                SidePosition.BOTTOM_RIGHT to SidePosition.LEFT_BOTTOM
            )
        )
        val drawingPile = DrawingPile(mutableListOf(card1, card2))

        val card = drawingPile.draw()

        assertEquals(card1, card)
        assertEquals(1, drawingPile.size())
    }

    @Test
    internal fun `shuffled the pile size should be the same`() {
        val drawingPile = DrawingPile()
        val pileSize = drawingPile.size()

        drawingPile.shuffle()

        assertEquals(drawingPile.size(), pileSize)
    }

    @Test
    internal fun `draw from 2 shuffled pile unlikely to have the same first draw (might fail)`() {
        val drawingPile1 = DrawingPile()
        val drawingPile2 = DrawingPile()

        drawingPile1.shuffle()
        drawingPile2.shuffle()

        val card1 = drawingPile1.draw()
        val card2 = drawingPile2.draw()
        assertNotEquals(card1, card2)
    }
}