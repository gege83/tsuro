package com.gergo.takacs.tsuro

import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.Matchers
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.mockito.kotlin.*
import kotlin.test.assertEquals

internal class GameTest {
    private val drawingPile = DrawingPile()

    @Test
    fun shouldAddTheNewPlayerToTheGame() {
        val gameName = "new game"
        val playerName = "player1"

        val game = Game(gameName, Board(2, 2), drawingPile)
        val newGame = game.addPlayer(Player(playerName, "@"))

        Assertions.assertEquals(listOf(Player(playerName, "@")), newGame.players)
        Assertions.assertEquals(gameName, newGame.name)
    }

    @Test
    fun shouldRandomizePlayers() {
        val player1 = Player("player1", "@")
        val player2 = Player("player2", "#")
        val player3 = Player("player3", "$")
        val game = Game("new game", Board(2, 2), drawingPile, listOf(player1, player2, player3))

        val gameRandomOrder = game.randomizePlayers()

        assertThat(gameRandomOrder.players, Matchers.containsInAnyOrder(player1, player2, player3))
        Assertions.assertEquals(3, game.players.size)
    }

    @Test
    fun shouldSetInitialMarkerPositionAndChangeActivePlayerToPlayer2() {
        val newMockBoard = mock<Board>()
        val mockBoard = mock<Board> {
            on { addMarker(any()) } doReturn newMockBoard
        }
        val player1 = Player("player1", "@")
        val player2 = Player("player2", "#")
        val gameName = "new game"
        val game = Game(gameName, mockBoard, drawingPile, listOf(player1, player2), 0)
        val newGame = game.placeInitialMarker(1, 0, SidePosition.TOP_LEFT)

        assertEquals(Game(gameName, newMockBoard, drawingPile, listOf(player1, player2), 1), newGame)
        verify(mockBoard).addMarker(MarkerPosition(1, 0, SidePosition.TOP_LEFT, "@"))
    }

    @Test
    fun shouldSetInitialMarkerPositionAndChangeActivePlayerToPlayer1() {
        val newMockBoard = mock<Board>()
        val mockBoard = mock<Board> {
            on { addMarker(any()) } doReturn newMockBoard
        }
        val player1 = Player("player1", "@")
        val player2 = Player("player2", "#")
        val gameName = "new game"
        val game = Game(gameName, mockBoard, drawingPile, listOf(player1, player2), 1)
        val newGame = game.placeInitialMarker(2, 0, SidePosition.TOP_RIGHT)

        assertEquals(Game(gameName, newMockBoard, drawingPile, listOf(player1, player2), 0), newGame)
        verify(mockBoard).addMarker(MarkerPosition(2, 0, SidePosition.TOP_RIGHT, "#"))
    }

    @Test
    fun shouldReturnPlayer1WhenActivePlayerIndexIs0() {
        val player1 = Player("player1", "@")
        val player2 = Player("player2", "#")
        val gameName = "new game"

        val game = Game(gameName, Board(2, 2), drawingPile, listOf(player1, player2), 0)

        assertEquals(player1, game.activePlayer())
    }

    @Test
    fun shouldReturnValidSidePositionFromTheBoard() {
        val expected = listOf(SidePosition.TOP_LEFT, SidePosition.TOP_RIGHT)
        val mockBoard = mock<Board> {
            on { getValidSidePositionsFor(any(), any()) } doReturn expected
        }
        val game = Game("name", mockBoard, drawingPile)
        val sides = game.getValidSidePositionsFor(2, 3)
        assertEquals(expected, sides)
        verify(mockBoard).getValidSidePositionsFor(2, 3)
    }

    @Test
    fun shouldDeal3CardsToAllPlayers() {
        val expected = listOf(SidePosition.TOP_LEFT, SidePosition.TOP_RIGHT)
        val mockBoard = mock<Board> {
            on { getValidSidePositionsFor(any(), any()) } doReturn expected
        }
        val card1 = Card(
            listOf(
                SidePosition.TOP_LEFT to SidePosition.BOTTOM_LEFT,
                SidePosition.TOP_RIGHT to SidePosition.RIGHT_TOP,
                SidePosition.RIGHT_BOTTOM to SidePosition.LEFT_BOTTOM,
                SidePosition.BOTTOM_RIGHT to SidePosition.LEFT_TOP
            )
        )
        val mockPile = mock<DrawingPile> {
            on { draw() } doReturn card1
        }
        val player1 = Player("p1", "#")
        val player2 = Player("p2", "$")
        val game = Game("name", mockBoard, mockPile, listOf(player1, player2))
        val gameWithCards = game.dealFirstHand()
        assertEquals(
            Game(
                "name",
                mockBoard,
                mockPile,
                listOf(
                    Player("p1", "#", listOf(card1, card1, card1)),
                    Player("p2", "$", listOf(card1, card1, card1)),
                )
            ),
            gameWithCards
        )
        verify(mockPile, times(6)).draw()
    }

    @Test
    internal fun `should rotate active player's hand clockwise`() {
        val tile1 = Card(listOf(Pair(SidePosition.TOP_RIGHT, SidePosition.LEFT_TOP)))
        val hand = listOf(tile1)
        val player1 = Player("p1", "#", hand)
        val game = Game("name", Board(2, 2, listOf()), drawingPile, listOf(player1))

        val result = game.rotateActivePlayersHand()

        val expectedHand = listOf(Card(listOf(Pair(SidePosition.RIGHT_BOTTOM, SidePosition.TOP_RIGHT))))
        assertEquals(expectedHand, result.players[result.activePlayerIndex].hand)
    }
}