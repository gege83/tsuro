package com.gergo.takacs.tsuro

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestFactory
import org.junit.jupiter.api.assertThrows

internal class BoardTest {

    @BeforeEach
    fun setUp() {
    }

    @Test
    fun shouldAddMarkerToPosition() {
        val board = Board(2, 2)
        val marker = MarkerPosition(0, 1, SidePosition.LEFT_BOTTOM, "@")
        val newBoard = board.addMarker(marker)
        assertEquals(Board(2, 2, listOf(marker)), newBoard)
    }

    private val validSidesFor3x3Board = listOf(
        Pair(1, 1) to emptyList(),
        Pair(10, 0) to emptyList(),
        Pair(-1, 0) to emptyList(),
        Pair(0, 10) to emptyList(),
        Pair(0, -1) to emptyList(),
        Pair(0, 1) to listOf(SidePosition.LEFT_BOTTOM, SidePosition.LEFT_TOP),
        Pair(2, 1) to listOf(SidePosition.RIGHT_TOP, SidePosition.RIGHT_BOTTOM),
        Pair(1, 0) to listOf(SidePosition.TOP_LEFT, SidePosition.TOP_RIGHT),
        Pair(1, 2) to listOf(SidePosition.BOTTOM_RIGHT, SidePosition.BOTTOM_LEFT),
        Pair(0, 0) to listOf(
            SidePosition.TOP_LEFT,
            SidePosition.TOP_RIGHT,
            SidePosition.LEFT_BOTTOM,
            SidePosition.LEFT_TOP
        ),
        Pair(2, 2) to listOf(
            SidePosition.RIGHT_TOP,
            SidePosition.RIGHT_BOTTOM,
            SidePosition.BOTTOM_RIGHT,
            SidePosition.BOTTOM_LEFT
        )
    )

    @TestFactory
    fun shouldShowAvailableSidesOnA3x3Board() = validSidesFor3x3Board
        .map { (input, expectedList) ->
            DynamicTest.dynamicTest("$expectedList when x:${input.first} y:${input.second}") {
                val board = Board(3, 3)
                val sides = board.getValidSidePositionsFor(input.first, input.second)
                assertEquals(expectedList, sides)
            }
        }

    @Test
    fun shouldNotShowSidePositionsWhichAreOccupied() {
        val board = Board(3, 3).addMarker(MarkerPosition(0, 0, SidePosition.TOP_LEFT, "@"))
        val sides = board.getValidSidePositionsFor(0, 0)
        assertEquals(
            listOf(
                SidePosition.TOP_RIGHT,
                SidePosition.LEFT_BOTTOM,
                SidePosition.LEFT_TOP
            ), sides
        )
    }

    private val tile = Card(
        listOf(
            SidePosition.TOP_LEFT to SidePosition.TOP_RIGHT,
            SidePosition.LEFT_TOP to SidePosition.LEFT_BOTTOM,
            SidePosition.RIGHT_TOP to SidePosition.RIGHT_BOTTOM,
            SidePosition.BOTTOM_LEFT to SidePosition.BOTTOM_RIGHT
        )
    )

    @Test
    fun shouldPlaceACardFirstRound() {
        val board = Board(3, 3, listOf(MarkerPosition(0, 0, SidePosition.TOP_LEFT, "@")))
        val boardWithTile = board.placeCard(tile, 0, 0, "@")
        assertEquals(tile, boardWithTile.tiles[0][0])
    }

    @Test
    fun shouldPlaceACardNextToCurrentPosition() {
        val board = Board(
            3,
            3,
            listOf(
                MarkerPosition(0, 0, SidePosition.TOP_LEFT, "@"),
                MarkerPosition(0, 0, SidePosition.BOTTOM_LEFT, "@")
            ),
            listOf(TilePosition(tile, 0, 0))
        )
        val boarWithNewTile = board.placeCard(tile, 1, 0, "@")
        assertEquals(tile, boarWithNewTile.tiles[1][0])
    }

    private val invalidPositionsFor3x3 = listOf(
        1 to 1,
        -1 to 0,
        0 to -1,
        10 to 0,
        0 to 10
    )

    @TestFactory
    fun invalidPositions() = invalidPositionsFor3x3.map { (x, y) ->
        DynamicTest.dynamicTest("should throw invalid position when x: $x y: $y") {
            val board = Board(3, 3, listOf(MarkerPosition(0, 0, SidePosition.TOP_LEFT, "@")))
            assertThrows<InvalidPositionException> { board.placeCard(tile, x, y, "@") }
        }
    }

    @Test
    fun shouldThrowWhenSymbolNotFound() {
        val board = Board(3, 3, listOf(MarkerPosition(0, 0, SidePosition.TOP_LEFT, "@")))
        assertThrows<InvalidPositionException> { board.placeCard(tile, 1, 0, "£") }
    }

    @Test
    fun shouldThrowWhenTileIsNotConnectedToCurrentPositionWhenFirstRound() {
        val board = Board(3, 3, listOf(MarkerPosition(0, 0, SidePosition.TOP_LEFT, "@")))
        assertThrows<InvalidPositionException> { board.placeCard(tile, 1, 0, "@") }
    }

    @Test
    fun shouldThrowWhenTileIsNotConnectedToCurrentPositionWhenSecondRound() {
        val board = Board(
            3,
            3,
            listOf(
                MarkerPosition(0, 0, SidePosition.TOP_LEFT, "@"),
                MarkerPosition(0, 0, SidePosition.BOTTOM_LEFT, "@")
            )
        )
        assertThrows<InvalidPositionException> { board.placeCard(tile, 1, 1, "@") }
    }

    @Test
    fun shouldThrowWhenTileIsAlreadyUsed() {
        val board = Board(
            3,
            3,
            listOf(MarkerPosition(0, 0, SidePosition.TOP_LEFT, "@")),
            listOf(TilePosition(tile, 0, 0))
        )
        assertThrows<InvalidPositionException> { board.placeCard(tile, 0, 0, "@") }
    }
}