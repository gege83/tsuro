package com.gergo.takacs.tsuro

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.TestFactory

internal class CardTest {

    private val tileRotations = listOf(
        Card(listOf()) to Card(listOf()),
        Card(listOf(SidePosition.TOP_RIGHT to SidePosition.TOP_LEFT)) to Card(listOf(SidePosition.RIGHT_BOTTOM to SidePosition.RIGHT_TOP)),
        Card(listOf(SidePosition.RIGHT_BOTTOM to SidePosition.RIGHT_TOP)) to Card(listOf(SidePosition.BOTTOM_LEFT to SidePosition.BOTTOM_RIGHT)),
        Card(listOf(SidePosition.BOTTOM_LEFT to SidePosition.BOTTOM_RIGHT)) to Card(listOf(SidePosition.LEFT_TOP to SidePosition.LEFT_BOTTOM)),
        Card(listOf(SidePosition.LEFT_TOP to SidePosition.LEFT_BOTTOM)) to Card(listOf(SidePosition.TOP_RIGHT to SidePosition.TOP_LEFT)),
        Card(
            listOf(
                SidePosition.TOP_LEFT to SidePosition.BOTTOM_LEFT,
                SidePosition.TOP_RIGHT to SidePosition.RIGHT_TOP,
                SidePosition.RIGHT_BOTTOM to SidePosition.LEFT_BOTTOM,
                SidePosition.BOTTOM_RIGHT to SidePosition.LEFT_TOP
            )
        ) to Card(
            listOf(
                SidePosition.RIGHT_TOP to SidePosition.LEFT_TOP,
                SidePosition.RIGHT_BOTTOM to SidePosition.BOTTOM_RIGHT,
                SidePosition.BOTTOM_LEFT to SidePosition.TOP_LEFT,
                SidePosition.LEFT_BOTTOM to SidePosition.TOP_RIGHT
            )
        ),
    )

    @TestFactory
    fun `should rotate card clockwise1`() = tileRotations
        .map { (card, rotatedCard) ->
            DynamicTest.dynamicTest("$card to $rotatedCard") {
                assertEquals(rotatedCard, card.rotate())
            }
        }
}