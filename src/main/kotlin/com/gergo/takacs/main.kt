package com.gergo.takacs

import com.gergo.takacs.tsuro.Game
import com.gergo.takacs.tsuro.console.*
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.PrintStream

fun main() {
    val inputStream = BufferedReader(InputStreamReader(System.`in`))
    val outputStream = PrintStream(System.out)
    initializeGame(inputStream, outputStream)
        .randomizePlayers()
        .initializeMarkers(inputStream, outputStream)
        .dealFirstHand()
        .renderGame(outputStream)
        .executeOptions(inputStream, outputStream)
        .renderGame(outputStream)
}

private fun Game.executeOptions(inputStream: BufferedReader, outputStream: PrintStream): Game {
    do {
        outputStream.println("menu:\n1 - rotate cards")
    } while (inputStream.readLine() != "1")
    outputStream.println("rotating...")
    return this.rotateActivePlayersHand()
}

fun Game.renderGame(outputStream: PrintStream): Game {
    val tileRenderer = TileRenderer()
    val printedGame = GameRenderer(BoardRenderer(tileRenderer), tileRenderer).renderGame(this)
    outputStream.print(printedGame)
    return this
}
