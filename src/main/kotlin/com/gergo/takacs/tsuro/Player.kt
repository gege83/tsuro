package com.gergo.takacs.tsuro

data class Player(val name: String, val marker: String, val hand: List<Card> = listOf())