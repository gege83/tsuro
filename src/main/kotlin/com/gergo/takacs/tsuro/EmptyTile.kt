package com.gergo.takacs.tsuro

data class EmptyTile(val text: String = "00") : Tile {
    override fun rotate(): Tile {
        return this
    }
}
