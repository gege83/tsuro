package com.gergo.takacs.tsuro

data class TilePosition(val tile: Tile, val x: Int, val y: Int)
