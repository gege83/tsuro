package com.gergo.takacs.tsuro

data class Card(val connections: List<Pair<SidePosition, SidePosition>>) : Tile {
    override fun rotate(): Card {
        return Card(this.connections.map { Pair(rotateSide(it.first), rotateSide(it.second)) })
    }

    private fun rotateSide(sidePosition: SidePosition) =
        SidePosition.values()[(sidePosition.ordinal + 2) % SidePosition.values().size]
}