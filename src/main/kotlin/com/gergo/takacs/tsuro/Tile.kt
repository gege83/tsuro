package com.gergo.takacs.tsuro

interface Tile {
    fun rotate(): Tile
}