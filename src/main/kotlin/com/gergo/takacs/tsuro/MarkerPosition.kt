package com.gergo.takacs.tsuro

data class MarkerPosition(val x: Int, val y: Int, val side: SidePosition, val symbol: String)

enum class SidePosition {
    TOP_LEFT,
    TOP_RIGHT,
    RIGHT_TOP,
    RIGHT_BOTTOM,
    BOTTOM_RIGHT,
    BOTTOM_LEFT,
    LEFT_BOTTOM,
    LEFT_TOP
}
