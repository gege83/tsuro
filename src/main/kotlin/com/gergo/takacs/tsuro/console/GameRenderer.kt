package com.gergo.takacs.tsuro.console

import com.gergo.takacs.tsuro.Card
import com.gergo.takacs.tsuro.Game
import com.gergo.takacs.tsuro.Player

class GameRenderer(private val boardRenderer: BoardRenderer, private val tileRenderer: TileRenderer) {
    fun renderGame(game: Game): String {
        return renderPlayers(game.players, game.activePlayerIndex) +
                "\n\n" +
                boardRenderer.renderBoard(game.board) +
                renderActivePlayerHand(game.activePlayer())
    }

    private fun renderActivePlayerHand(activePlayer: Player): String {
        if (activePlayer.hand.isEmpty()) {
            return ""
        }
        return "\n\n${activePlayer.name}'s hand:\n${renderTiles(activePlayer.hand)}\n"
    }

    private fun renderTiles(hand: List<Card>): String {
        return hand.map { tileRenderer.renderTile(it, listOf()) }
            .reduce { acc, element -> acc.zip(element).map { it.first + it.second } }
            .joinToString("\n")
    }

    private fun renderPlayers(players: List<Player>, activeIndex: Int): String {
        val playerList = players
            .mapIndexed { index, player ->
                if (index == activeIndex) {
                    "-> ${player.marker} ${player.name}"
                } else {
                    "   ${player.marker} ${player.name}"
                }
            }.joinToString("\n")
        return "Players:\n$playerList"
    }
}