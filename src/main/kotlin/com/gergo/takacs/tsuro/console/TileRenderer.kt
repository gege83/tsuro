package com.gergo.takacs.tsuro.console

import com.gergo.takacs.tsuro.*

class TileRenderer {
    fun renderTile(tile: Tile, markerPositions: List<MarkerPosition>, text: String = "  "): List<String> {
        return when (tile) {
            is EmptyTile -> renderEmptyTile(text, markerPositions)
            is Card -> renderCard(tile)
            else -> listOf()
        }
    }

    private fun renderCard(tile: Card): List<String> {
        val connections = tile.connections
        val topLeft = getConnectionIndex(connections, SidePosition.TOP_LEFT)
        val topRight = getConnectionIndex(connections, SidePosition.TOP_RIGHT)
        val bottomLeft = getConnectionIndex(connections, SidePosition.BOTTOM_LEFT)
        val bottomRight = getConnectionIndex(connections, SidePosition.BOTTOM_RIGHT)
        val leftTop = getConnectionIndex(connections, SidePosition.LEFT_TOP)
        val rightTop = getConnectionIndex(connections, SidePosition.RIGHT_TOP)
        val leftBottom = getConnectionIndex(connections, SidePosition.LEFT_BOTTOM)
        val rightBottom = getConnectionIndex(connections, SidePosition.RIGHT_BOTTOM)
        return listOf(
            "┌───$topLeft───$topRight───┐",
            "│           │",
            "$leftTop           $rightTop",
            "│           │",
            "$leftBottom           $rightBottom",
            "│           │",
            "└───$bottomLeft───$bottomRight───┘"
        )
    }

    private fun getConnectionIndex(
        connections: List<Pair<SidePosition, SidePosition>>,
        sidePosition: SidePosition
    ): String {
        return (connections.indexOfFirst { it.first == sidePosition || it.second == sidePosition } + 1).toString()
    }

    private fun renderEmptyTile(text: String, markerPositions: List<MarkerPosition>): List<String> {
        val topLeft = getMarkerAtSidePosition(markerPositions, SidePosition.TOP_LEFT)
        val topRight = getMarkerAtSidePosition(markerPositions, SidePosition.TOP_RIGHT)
        val bottomLeft = getMarkerAtSidePosition(markerPositions, SidePosition.BOTTOM_LEFT)
        val bottomRight = getMarkerAtSidePosition(markerPositions, SidePosition.BOTTOM_RIGHT)
        val leftTop = getMarkerAtSidePosition(markerPositions, SidePosition.LEFT_TOP)
        val rightTop = getMarkerAtSidePosition(markerPositions, SidePosition.RIGHT_TOP)
        val leftBottom = getMarkerAtSidePosition(markerPositions, SidePosition.LEFT_BOTTOM)
        val rightBottom = getMarkerAtSidePosition(markerPositions, SidePosition.RIGHT_BOTTOM)
        return listOf(
            "┌───$topLeft───$topRight───┐",
            "│           │",
            "$leftTop           $rightTop",
            "│    $text     │",
            "$leftBottom           $rightBottom",
            "│           │",
            "└───$bottomLeft───$bottomRight───┘"
        )
    }

    private fun getMarkerAtSidePosition(
        markerPositions: List<MarkerPosition>,
        sidePosition: SidePosition,
        defaultSymbol: String = "┼"
    ): String {
        val find = markerPositions.find { it.side == sidePosition }
        return find?.symbol ?: defaultSymbol
    }
}