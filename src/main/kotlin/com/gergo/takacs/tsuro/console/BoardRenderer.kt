package com.gergo.takacs.tsuro.console

import com.gergo.takacs.tsuro.Board

class BoardRenderer(private val tileRenderer: TileRenderer) {
    fun renderBoard(board: Board): String {
        return board.tiles.mapIndexed { rowIndex, row ->
            row.mapIndexed { columnIndex, tile ->
                val markersOnTile = board.markerPositions.filter { it.x == columnIndex && it.y == rowIndex }
                val text = calculateText(columnIndex, rowIndex)
                tileRenderer.renderTile(tile, markersOnTile, text)
            }.reduce { acc, element -> acc.zip(element).map { it.first + it.second } }
        }.joinToString("\n") {
            it.joinToString("\n")
        } + "\n"
    }

    private fun calculateText(x: Int, y: Int): String {
        val startChar = 'A'
        return (startChar + x).toString() + (y + 1)
    }
}