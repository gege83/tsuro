package com.gergo.takacs.tsuro.console

import com.gergo.takacs.renderGame
import com.gergo.takacs.tsuro.*
import java.io.BufferedReader
import java.io.PrintStream

fun initializeGame(inputStream: BufferedReader, outputStream: PrintStream): Game {
    val availableMarkerSymbols = "@#$%^&*!".toCharArray().map { it.toString() }
    outputStream.println("Please enter the game name:")
    val gameName = inputStream.readLine()
    var numberOfPlayer = 1
    var playerName: String
    var game = Game(gameName, Board(6, 6), DrawingPile())
    do {
        outputStream.println("Please enter the player$numberOfPlayer's name")
        playerName = inputStream.readLine()
        if (playerName.isNotBlank()) {
            game = game.addPlayer(
                Player(
                    playerName,
                    availableMarkerSymbols[numberOfPlayer - 1]
                )
            )
        }
        numberOfPlayer++
    } while (playerName.isNotBlank())
    return game
}

fun Game.initializeMarkers(inputReader: BufferedReader, printStream: PrintStream): Game {
    return this.players.fold(this) { g, player ->
        var x: Int
        var y: Int
        var sidePosition: SidePosition?
        g.renderGame(printStream)
        do {
            printStream.println("${player.name} select the tile for your marker")
            val tile = inputReader.readLine().uppercase()
            x = tile[0] - 'A'
            y = tile[1] - '1'
            sidePosition = this.readSidePosition(x, y, inputReader, printStream)
        } while (sidePosition == null)
        g.placeInitialMarker(x, y, sidePosition)
    }
}

private fun printSidePositions(sides: List<SidePosition>, printStream: PrintStream) =
    sides.forEach { printStream.println("${it.ordinal + 1}. ${it.name}") }

private fun Game.readSidePosition(
    x: Int,
    y: Int,
    inputReader: BufferedReader,
    printStream: PrintStream
): SidePosition? {
    val sides = this.getValidSidePositionsFor(x, y)
    if (sides.isEmpty()) {
        printStream.println("No valid move possible on the tile")
        return null
    }
    printStream.println("please select side position")
    printSidePositions(sides, printStream)
    val sidePositionOrdinal = inputReader.readLine().toInt() - 1
    if (sidePositionOrdinal < SidePosition.values().size) {
        val potentialPosition = SidePosition.values()[sidePositionOrdinal]
        if (sides.contains(potentialPosition)) {
            return potentialPosition
        }
    }
    printStream.println("selected position is not valid")
    return null
}
