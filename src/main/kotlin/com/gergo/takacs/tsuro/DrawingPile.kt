package com.gergo.takacs.tsuro

class DrawingPile(
    private val cards: MutableList<Card> = mutableListOf(
        Card(
            listOf(
                SidePosition.TOP_LEFT to SidePosition.BOTTOM_LEFT,
                SidePosition.TOP_RIGHT to SidePosition.RIGHT_TOP,
                SidePosition.RIGHT_BOTTOM to SidePosition.LEFT_BOTTOM,
                SidePosition.BOTTOM_RIGHT to SidePosition.LEFT_TOP
            )
        ),
        Card(
            listOf(
                SidePosition.TOP_LEFT to SidePosition.RIGHT_TOP,
                SidePosition.TOP_RIGHT to SidePosition.BOTTOM_LEFT,
                SidePosition.RIGHT_BOTTOM to SidePosition.LEFT_TOP,
                SidePosition.BOTTOM_RIGHT to SidePosition.LEFT_BOTTOM
            )
        ),
        Card(
            listOf(
                SidePosition.TOP_LEFT to SidePosition.RIGHT_TOP,
                SidePosition.TOP_RIGHT to SidePosition.BOTTOM_RIGHT,
                SidePosition.RIGHT_BOTTOM to SidePosition.LEFT_TOP,
                SidePosition.BOTTOM_LEFT to SidePosition.LEFT_BOTTOM
            )
        ),
        Card(
            listOf(
                SidePosition.TOP_LEFT to SidePosition.RIGHT_TOP,
                SidePosition.TOP_RIGHT to SidePosition.BOTTOM_LEFT,
                SidePosition.RIGHT_BOTTOM to SidePosition.LEFT_BOTTOM,
                SidePosition.BOTTOM_RIGHT to SidePosition.LEFT_TOP
            )
        ),
        Card(
            listOf(
                SidePosition.TOP_LEFT to SidePosition.LEFT_BOTTOM,
                SidePosition.TOP_RIGHT to SidePosition.LEFT_TOP,
                SidePosition.RIGHT_TOP to SidePosition.BOTTOM_RIGHT,
                SidePosition.RIGHT_BOTTOM to SidePosition.BOTTOM_LEFT
            )
        ),
        Card(
            listOf(
                SidePosition.TOP_LEFT to SidePosition.TOP_RIGHT,
                SidePosition.RIGHT_TOP to SidePosition.BOTTOM_LEFT,
                SidePosition.RIGHT_BOTTOM to SidePosition.LEFT_BOTTOM,
                SidePosition.BOTTOM_RIGHT to SidePosition.LEFT_TOP
            )
        ),

        Card(
            listOf(
                SidePosition.TOP_LEFT to SidePosition.TOP_RIGHT,
                SidePosition.RIGHT_TOP to SidePosition.LEFT_TOP,
                SidePosition.RIGHT_BOTTOM to SidePosition.BOTTOM_LEFT,
                SidePosition.BOTTOM_RIGHT to SidePosition.LEFT_BOTTOM
            )
        ),
        Card(
            listOf(
                SidePosition.TOP_LEFT to SidePosition.BOTTOM_RIGHT,
                SidePosition.TOP_RIGHT to SidePosition.BOTTOM_LEFT,
                SidePosition.RIGHT_TOP to SidePosition.LEFT_BOTTOM,
                SidePosition.RIGHT_BOTTOM to SidePosition.LEFT_TOP
            )
        ),
        Card(
            listOf(
                SidePosition.TOP_LEFT to SidePosition.BOTTOM_LEFT,
                SidePosition.TOP_RIGHT to SidePosition.BOTTOM_RIGHT,
                SidePosition.RIGHT_TOP to SidePosition.LEFT_TOP,
                SidePosition.RIGHT_BOTTOM to SidePosition.LEFT_BOTTOM
            )
        ),
        Card(
            listOf(
                SidePosition.TOP_LEFT to SidePosition.BOTTOM_RIGHT,
                SidePosition.TOP_RIGHT to SidePosition.LEFT_BOTTOM,
                SidePosition.RIGHT_TOP to SidePosition.LEFT_TOP,
                SidePosition.RIGHT_BOTTOM to SidePosition.BOTTOM_LEFT
            )
        ),
        Card(
            listOf(
                SidePosition.TOP_LEFT to SidePosition.BOTTOM_LEFT,
                SidePosition.TOP_RIGHT to SidePosition.BOTTOM_RIGHT,
                SidePosition.RIGHT_TOP to SidePosition.RIGHT_BOTTOM,
                SidePosition.LEFT_BOTTOM to SidePosition.LEFT_TOP
            )
        ),
        Card(
            listOf(
                SidePosition.TOP_LEFT to SidePosition.LEFT_BOTTOM,
                SidePosition.TOP_RIGHT to SidePosition.LEFT_TOP,
                SidePosition.RIGHT_TOP to SidePosition.BOTTOM_LEFT,
                SidePosition.RIGHT_BOTTOM to SidePosition.BOTTOM_RIGHT
            )
        ),

        Card(
            listOf(
                SidePosition.TOP_LEFT to SidePosition.LEFT_TOP,
                SidePosition.TOP_RIGHT to SidePosition.LEFT_BOTTOM,
                SidePosition.RIGHT_TOP to SidePosition.BOTTOM_LEFT,
                SidePosition.RIGHT_BOTTOM to SidePosition.BOTTOM_RIGHT
            )
        ),
        Card(
            listOf(
                SidePosition.TOP_LEFT to SidePosition.RIGHT_TOP,
                SidePosition.TOP_RIGHT to SidePosition.BOTTOM_RIGHT,
                SidePosition.RIGHT_BOTTOM to SidePosition.BOTTOM_LEFT,
                SidePosition.LEFT_BOTTOM to SidePosition.LEFT_TOP
            )
        ),
        Card(
            listOf(
                SidePosition.TOP_LEFT to SidePosition.LEFT_TOP,
                SidePosition.TOP_RIGHT to SidePosition.RIGHT_TOP,
                SidePosition.RIGHT_BOTTOM to SidePosition.BOTTOM_RIGHT,
                SidePosition.BOTTOM_LEFT to SidePosition.LEFT_BOTTOM
            )
        ),
        Card(
            listOf(
                SidePosition.TOP_LEFT to SidePosition.BOTTOM_LEFT,
                SidePosition.TOP_RIGHT to SidePosition.BOTTOM_RIGHT,
                SidePosition.RIGHT_TOP to SidePosition.LEFT_BOTTOM,
                SidePosition.RIGHT_BOTTOM to SidePosition.LEFT_TOP
            )
        ),
        Card(
            listOf(
                SidePosition.TOP_LEFT to SidePosition.RIGHT_TOP,
                SidePosition.TOP_RIGHT to SidePosition.RIGHT_BOTTOM,
                SidePosition.BOTTOM_RIGHT to SidePosition.BOTTOM_LEFT,
                SidePosition.LEFT_BOTTOM to SidePosition.LEFT_TOP
            )
        ),
        Card(
            listOf(
                SidePosition.TOP_LEFT to SidePosition.BOTTOM_LEFT,
                SidePosition.TOP_RIGHT to SidePosition.LEFT_TOP,
                SidePosition.RIGHT_TOP to SidePosition.LEFT_BOTTOM,
                SidePosition.RIGHT_BOTTOM to SidePosition.BOTTOM_RIGHT
            )
        ),

        Card(
            listOf(
                SidePosition.TOP_LEFT to SidePosition.RIGHT_BOTTOM,
                SidePosition.TOP_RIGHT to SidePosition.LEFT_BOTTOM,
                SidePosition.RIGHT_TOP to SidePosition.BOTTOM_LEFT,
                SidePosition.BOTTOM_LEFT to SidePosition.LEFT_TOP
            )
        ),
        Card(
            listOf(
                SidePosition.TOP_LEFT to SidePosition.TOP_RIGHT,
                SidePosition.RIGHT_TOP to SidePosition.LEFT_BOTTOM,
                SidePosition.RIGHT_BOTTOM to SidePosition.LEFT_TOP,
                SidePosition.BOTTOM_LEFT to SidePosition.BOTTOM_RIGHT
            )
        ),
        Card(
            listOf(
                SidePosition.TOP_LEFT to SidePosition.LEFT_TOP,
                SidePosition.TOP_RIGHT to SidePosition.RIGHT_BOTTOM,
                SidePosition.RIGHT_TOP to SidePosition.BOTTOM_RIGHT,
                SidePosition.BOTTOM_LEFT to SidePosition.LEFT_BOTTOM
            )
        ),
        Card(
            listOf(
                SidePosition.TOP_LEFT to SidePosition.TOP_RIGHT,
                SidePosition.RIGHT_TOP to SidePosition.LEFT_TOP,
                SidePosition.RIGHT_BOTTOM to SidePosition.BOTTOM_LEFT,
                SidePosition.BOTTOM_RIGHT to SidePosition.LEFT_BOTTOM
            )
        ),
        Card(
            listOf(
                SidePosition.TOP_LEFT to SidePosition.LEFT_BOTTOM,
                SidePosition.TOP_RIGHT to SidePosition.RIGHT_BOTTOM,
                SidePosition.RIGHT_TOP to SidePosition.BOTTOM_LEFT,
                SidePosition.BOTTOM_RIGHT to SidePosition.LEFT_TOP
            )
        ),
        Card(
            listOf(
                SidePosition.TOP_LEFT to SidePosition.LEFT_TOP,
                SidePosition.TOP_RIGHT to SidePosition.RIGHT_BOTTOM,
                SidePosition.RIGHT_TOP to SidePosition.LEFT_BOTTOM,
                SidePosition.BOTTOM_RIGHT to SidePosition.BOTTOM_LEFT
            )
        ),

        Card(
            listOf(
                SidePosition.TOP_LEFT to SidePosition.LEFT_BOTTOM,
                SidePosition.TOP_RIGHT to SidePosition.BOTTOM_LEFT,
                SidePosition.RIGHT_TOP to SidePosition.LEFT_BOTTOM,
                SidePosition.BOTTOM_RIGHT to SidePosition.LEFT_TOP
            )
        ),
        Card(
            listOf(
                SidePosition.TOP_LEFT to SidePosition.RIGHT_BOTTOM,
                SidePosition.TOP_RIGHT to SidePosition.RIGHT_TOP,
                SidePosition.BOTTOM_RIGHT to SidePosition.BOTTOM_LEFT,
                SidePosition.LEFT_BOTTOM to SidePosition.LEFT_TOP
            )
        ),
        Card(
            listOf(
                SidePosition.TOP_LEFT to SidePosition.BOTTOM_RIGHT,
                SidePosition.TOP_RIGHT to SidePosition.LEFT_TOP,
                SidePosition.RIGHT_TOP to SidePosition.LEFT_BOTTOM,
                SidePosition.RIGHT_BOTTOM to SidePosition.BOTTOM_LEFT
            )
        ),
        Card(
            listOf(
                SidePosition.TOP_LEFT to SidePosition.LEFT_TOP,
                SidePosition.TOP_RIGHT to SidePosition.BOTTOM_LEFT,
                SidePosition.RIGHT_TOP to SidePosition.RIGHT_BOTTOM,
                SidePosition.BOTTOM_RIGHT to SidePosition.LEFT_BOTTOM
            )
        ),
        Card(
            listOf(
                SidePosition.TOP_LEFT to SidePosition.RIGHT_TOP,
                SidePosition.TOP_RIGHT to SidePosition.BOTTOM_RIGHT,
                SidePosition.RIGHT_BOTTOM to SidePosition.LEFT_BOTTOM,
                SidePosition.BOTTOM_LEFT to SidePosition.LEFT_TOP
            )
        ),
        Card(
            listOf(
                SidePosition.TOP_LEFT to SidePosition.LEFT_BOTTOM,
                SidePosition.TOP_RIGHT to SidePosition.RIGHT_BOTTOM,
                SidePosition.RIGHT_TOP to SidePosition.BOTTOM_RIGHT,
                SidePosition.BOTTOM_LEFT to SidePosition.LEFT_TOP
            )
        ),

        Card(
            listOf(
                SidePosition.TOP_LEFT to SidePosition.RIGHT_BOTTOM,
                SidePosition.TOP_RIGHT to SidePosition.LEFT_TOP,
                SidePosition.RIGHT_TOP to SidePosition.BOTTOM_RIGHT,
                SidePosition.BOTTOM_LEFT to SidePosition.LEFT_BOTTOM
            )
        ),
        Card(
            listOf(
                SidePosition.TOP_LEFT to SidePosition.BOTTOM_RIGHT,
                SidePosition.TOP_RIGHT to SidePosition.LEFT_BOTTOM,
                SidePosition.RIGHT_TOP to SidePosition.RIGHT_BOTTOM,
                SidePosition.BOTTOM_LEFT to SidePosition.LEFT_TOP
            )
        ),
        Card(
            listOf(
                SidePosition.TOP_LEFT to SidePosition.LEFT_TOP,
                SidePosition.TOP_RIGHT to SidePosition.BOTTOM_LEFT,
                SidePosition.RIGHT_TOP to SidePosition.LEFT_BOTTOM,
                SidePosition.RIGHT_BOTTOM to SidePosition.BOTTOM_RIGHT
            )
        ),
        Card(
            listOf(
                SidePosition.TOP_LEFT to SidePosition.TOP_RIGHT,
                SidePosition.RIGHT_TOP to SidePosition.BOTTOM_LEFT,
                SidePosition.RIGHT_BOTTOM to SidePosition.LEFT_TOP,
                SidePosition.BOTTOM_RIGHT to SidePosition.LEFT_BOTTOM
            )
        ),
        Card(
            listOf(
                SidePosition.TOP_LEFT to SidePosition.TOP_RIGHT,
                SidePosition.RIGHT_TOP to SidePosition.RIGHT_BOTTOM,
                SidePosition.BOTTOM_RIGHT to SidePosition.BOTTOM_LEFT,
                SidePosition.LEFT_BOTTOM to SidePosition.LEFT_TOP
            )
        )
    )
) {

    fun draw(): Card {
        val result = cards[0]
        cards.removeAt(0)
        return result
    }

    fun size(): Int = cards.size
    fun shuffle() {
        cards.shuffle()
    }
}
