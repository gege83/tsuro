package com.gergo.takacs.tsuro

data class Game(
    val name: String,
    val board: Board,
    val drawingPile: DrawingPile,
    val players: List<Player> = listOf(),
    val activePlayerIndex: Int = 0
) {
    fun addPlayer(player: Player): Game = Game(name, board, drawingPile, players + player)
    fun randomizePlayers(): Game = Game(name, board, drawingPile, players.shuffled())
    fun activePlayer(): Player = players[activePlayerIndex]
    fun placeInitialMarker(x: Int, y: Int, position: SidePosition): Game {
        val symbol = activePlayer().marker
        val markerPosition = MarkerPosition(x, y, position, symbol)
        return Game(name, board.addMarker(markerPosition), drawingPile, players, (activePlayerIndex + 1) % players.size)
    }

    fun getValidSidePositionsFor(x: Int, y: Int): List<SidePosition> {
        return board.getValidSidePositionsFor(x, y)
    }

    fun dealFirstHand(): Game {
        val playersWithHand = players.map {
            val hand = listOf(drawingPile.draw(), drawingPile.draw(), drawingPile.draw())
            Player(it.name, it.marker, hand)
        }
        return Game(name, board, drawingPile, playersWithHand, activePlayerIndex)
    }

    fun rotateActivePlayersHand(): Game {
        val playersWithNewHand = players.mapIndexed { index, player ->
            if (index == activePlayerIndex) Player(player.name, player.marker, player.hand.map { it.rotate() })
            else player
        }
        return Game(name, board, drawingPile, playersWithNewHand, activePlayerIndex)
    }
}
