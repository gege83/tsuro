package com.gergo.takacs.tsuro

import kotlin.math.abs

data class Board(
    val width: Int,
    val height: Int,
    val markerPositions: List<MarkerPosition> = listOf(),
    val tilePositions: List<TilePosition> = listOf()
) {
    val tiles: Array<Array<Tile>> = Array(height) { rowIndex ->
        Array(width) { columnIndex ->
            val char = 'A'
            val card =
                tilePositions.find { tilePosition -> tilePosition.x == rowIndex && tilePosition.y == columnIndex }
            card?.tile ?: EmptyTile((char + rowIndex).toString() + (columnIndex + 1))
        }
    }

    fun addMarker(marker: MarkerPosition): Board {
        return Board(width, height, markerPositions + marker)
    }

    fun getValidSidePositionsFor(x: Int, y: Int): List<SidePosition> {
        if (isPositionOutsideTheBoard(x, y)) {
            return emptyList()
        }
        val occupiedPositionOnTile = markerPositions.filter { it.x == x && it.y == y }.map { it.side }
        val horizontal = when (x) {
            0 -> listOf(SidePosition.LEFT_BOTTOM, SidePosition.LEFT_TOP)
            width - 1 -> listOf(SidePosition.RIGHT_TOP, SidePosition.RIGHT_BOTTOM)
            else -> emptyList()
        }
        val vertical = when (y) {
            0 -> listOf(SidePosition.TOP_LEFT, SidePosition.TOP_RIGHT)
            height - 1 -> listOf(SidePosition.BOTTOM_RIGHT, SidePosition.BOTTOM_LEFT)
            else -> emptyList()
        }
        return (vertical + horizontal)
            .filter { !occupiedPositionOnTile.contains(it) }
            .sorted()
    }

    fun placeCard(tile: Tile, x: Int, y: Int, symbol: String): Board {

        if (!isPositionOnTheEdgeBoard(x, y)
            || isPositionOutsideTheBoard(x, y)
            || isPositionUsed(x, y)
            || !nextToCurrentPosition(x, y, symbol)
        ) {
            throw InvalidPositionException()
        }

        return Board(width, height, markerPositions, tilePositions + TilePosition(tile, x, y))
    }

    private fun isPositionOutsideTheBoard(x: Int, y: Int) = x >= width || y >= height || x < 0 || y < 0
    private fun isPositionOnTheEdgeBoard(x: Int, y: Int) = x == 0 || y == 0 || x == width - 1 || y == height - 1

    private fun isPositionUsed(x: Int, y: Int) = tilePositions.any { it.x == x && it.y == y }

    private fun nextToCurrentPosition(x: Int, y: Int, symbol: String): Boolean {
        val symbolPositions = markerPositions.filter { it.symbol == symbol }
        return if (symbolPositions.isEmpty()) {
            false
        } else if (symbolPositions.size == 1) {
            val markerPosition = symbolPositions.first()
            markerPosition.x == x && markerPosition.y == y
        } else {
            val markerPosition = symbolPositions.last()
            (abs(markerPosition.x - x) == 1) xor (abs(markerPosition.y - y) == 1)
        }
    }
}

class InvalidPositionException : Throwable()